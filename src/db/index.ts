import mongodb from 'mongodb';
import dotenv from 'dotenv';

dotenv.config({ path: '.env' });

export default async function makeDb () {
  const MongoClient = mongodb.MongoClient
  const url = `${process.env.MONGO_ATLAS_CONNECTION}`
  const dbName = 'practice-simple-shop'
  const client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true})
  await client.connect()
  const db: any = await client.db(dbName) 
  db.makeId = makeIdFromString
  console.info(`Connected to db...`)
  return db  
}

function makeIdFromString (id: string) {
  return new mongodb.ObjectID(id)
}


