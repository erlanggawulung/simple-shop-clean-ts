import bcrypt from 'bcrypt'
import requiredParam from '../helper/requiredParam'
import { InvalidPropertyError } from '../helper/error'
import IUser from './userInterface'

export default function makeUser({
  userInfo = requiredParam('userInfo')
}: any) {
  const validUser = validate(userInfo);
  const normalizeUser = normalize(validUser);
  return Object.freeze(normalizeUser);

  function validate({
    email = requiredParam('email'),
    password = requiredParam('password')
  }: any = {}) {
    email = validateEmail('email', email)
    password = validatePassword('password', password)
    return { email, password }
  }

  function validateEmail (label: string, email: string) {
    const match = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    if (!email.match(match)) {
      throw new InvalidPropertyError(
        `A user's ${label} must be in valid email format.`
      )
    }
    return email;
  }

  function validatePassword (label: string, password: string) {
    const limit = 4
    if(password.length < limit) {
      throw new InvalidPropertyError(
        `A user's ${label} must be at least ${limit} characters.`
      )
    }
    return bcrypt.hashSync(password, 10);
  }

  function normalize ({ email, ...otherInfo }: IUser) {
    return {
      ...otherInfo,
      email: email.toLowerCase()
    }
  }
}