import IUser from './userInterface'
import { InvalidPropertyError } from '../helper/error'

export default function makeProductList({ database }: any) {
  return Object.freeze({
    add,
    findByEmail
  });

  async function add ({...user }: IUser) {
    const db = await database
    const found = await findByEmail(user)
    if (found) {
      throw new InvalidPropertyError(
        `A user's email already exist.`
      ) 
    }

    const { result, ops } = await db
      .collection('users')
      .insertOne(user)
      .catch((mongoError: any) => {
        throw mongoError
      })
    return {
      success: result.ok === 1,
      created: documentToUser(ops[0])
    }
  }

  async function findByEmail ({ email }: IUser) {
    const db = await database
    const found = await db
      .collection('users')
      .findOne({ email })
    if (found) {
      return documentToUser(found)
    }
    return null
  }

  function documentToUser ({ ...doc }: IUser) {
    return doc;
  }
}