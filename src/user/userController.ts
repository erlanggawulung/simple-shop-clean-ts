import {Request, Response} from 'express';
import adaptRequest from '../helper/adaptRequest';
import handleUserRequest from '.';

export default async function userController (req: Request, res: Response) {
  const httpRequest = adaptRequest(req)
  try {
    const { headers, statusCode, data } = await handleUserRequest(httpRequest)
    return res
        .set(headers)
        .status(statusCode)
        .send(data)
  } catch (e) {
    console.error(e)
    return res.status(500).end()
  }
}