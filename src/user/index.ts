import makeDb from '../db'
import makeUserList from './userList'
import makeUserEndPointHandler from './userEndPoint'

const database = makeDb()
const userList = makeUserList({ database })
const userEndPointHandler = makeUserEndPointHandler({ userList })

export default userEndPointHandler
