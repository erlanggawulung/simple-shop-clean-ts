import {
  UniqueConstraintError,
  InvalidPropertyError,
  RequiredParameterError
} from '../helper/error'
import makeHttpError from '../helper/httpError'
import makeUser from './user'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

export default function makeUsersEndpointHandler ({ userList }: any) {
  return async function handle (httpRequest: any) {
    switch (httpRequest.method) {
      case 'POST':
        return postUser(httpRequest)
      
      case 'GET':
        return getProfile(httpRequest)

      default:
        return makeHttpError({
          statusCode: 405,
          errorMessage: `${httpRequest.method} method not allowed.`
        })
    }
  }

  async function postUser(httpRequest: any) {
    const path = httpRequest.path;
    if(path == '/signup') {
      return await userSignup(httpRequest);
    } else if (path == '/login') {
      return await userLogin(httpRequest)
    }
    return makeHttpError({
      statusCode: 400,
      errorMessage: 'Bad request. Invalid path.'
    })
  }

  async function userSignup (httpRequest: any) {
    let userInfo = httpRequest.body
    if (!userInfo) {
      return makeHttpError({
        statusCode: 400,
        errorMessage: 'Bad request. No POST body.'
      })
    }

    if (typeof httpRequest.body === 'string') {
      try {
        userInfo = JSON.parse(userInfo)
      } catch {
        return makeHttpError({
          statusCode: 400,
          errorMessage: 'Bad request. POST body must be valid JSON.'
        })
      }
    }

    try {
      const userInfo = httpRequest.body
      const user = makeUser({userInfo})
      const result = await userList.add(user)
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 201,
        data: JSON.stringify(result)
      }
    } catch (e) {
      return makeHttpError({
        errorMessage: e.message,
        statusCode:
          e instanceof UniqueConstraintError
            ? 409
            : e instanceof InvalidPropertyError ||
              e instanceof RequiredParameterError
              ? 400
              : 500
      })
    }
  }

  async function userLogin(httpRequest: any) {
    let userInfo = httpRequest.body
    if (!userInfo) {
      return makeHttpError({
        statusCode: 400,
        errorMessage: 'Bad request. No POST body.'
      })
    }

    if (typeof httpRequest.body === 'string') {
      try {
        userInfo = JSON.parse(userInfo)
      } catch {
        return makeHttpError({
          statusCode: 400,
          errorMessage: 'Bad request. POST body must be valid JSON.'
        })
      }
    }

    try {
      const userInfo = httpRequest.body
      const user = makeUser({userInfo})
      const {email, password} = userInfo
      const existingUser = await userList.findByEmail(user)
      if(!existingUser) {
        throw new Error('Login failed');
      }
      const isLoginSuccess = bcrypt.compareSync(password, existingUser.password)
      if (!isLoginSuccess) {
        throw new Error('Login failed');
      }
      const token = jwt.sign({email: existingUser.email, id: existingUser._id},`${process.env.JWT_KEY}`, {expiresIn: '1h'})
      const result = { mesage: 'Auth successful', token}
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 200,
        data: JSON.stringify(result)
      }
    } catch (e) {
      return makeHttpError({
        errorMessage: e.message,
        statusCode:
          e instanceof UniqueConstraintError
            ? 409
            : e instanceof InvalidPropertyError ||
              e instanceof RequiredParameterError
              ? 400
              : 500
      })
    }
  }

  async function getProfile(httpRequest: any) {
    const {userData} = httpRequest;
    return {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 200,
      data: JSON.stringify(userData)
    }
  }
}