import express from 'express';
import checkAuth from '../middleware/checkAuth';
import userController from './userController';

const router = express.Router();

router.post('/signup', userController);
router.post('/login', userController);
router.get('/profile', checkAuth, userController);

export default router;