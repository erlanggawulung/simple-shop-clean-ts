import express, {Application, Request, Response} from 'express';
import bodyParser from 'body-parser';

import productRoutes from './product/productRoute';
import userRoutes from './user/userRoute';

const app: Application = express()
app.use(bodyParser.json())

app.use('/products', productRoutes);
app.use('/users', userRoutes);

export default app;
