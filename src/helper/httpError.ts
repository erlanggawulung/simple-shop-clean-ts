interface IHttpError {
  statusCode: number,
  errorMessage: string
}

export default function makeHttpError ({ statusCode, errorMessage }: IHttpError) {
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    statusCode,
    data: JSON.stringify({
      success: false,
      error: errorMessage
    })
  }
}
