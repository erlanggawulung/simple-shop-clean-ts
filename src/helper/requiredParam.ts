import { RequiredParameterError } from './error'

export default function requiredParam (param: string) {
  throw new RequiredParameterError(param)
}
