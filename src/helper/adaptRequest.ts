import {Request} from 'express';

export default function adaptRequest (req: any) {
	return Object.freeze({
		headers: req.headers,
		file: req.file,
		path: req.path,
		method: req.method,
		pathParams: req.params,
		queryParams: req.query,
		body: req.body,
		userData: req.userData
	})
}
  