import requiredParam from '../helper/requiredParam'
import { InvalidPropertyError } from '../helper/error'
import IProduct from './productInterface'

export default function makeProduct({
  productInfo = requiredParam('productInfo')  
}: any) {
  const validProduct = validate(productInfo);
  const normalizeProduct = normalize(validProduct);
  return Object.freeze(normalizeProduct);

  function validate({
    name = requiredParam('name'),
    price = requiredParam('price'),
    productImage = requiredParam('productImage')
  }: any = {}) {
    name = validateName('name', name)
    productImage = validateName('productImage', productImage)
    price = validatePrice('price', price)
    return { name, price, productImage }
  }

  function validateName (label: string, name: string) {
    if (name.length < 2) {
      throw new InvalidPropertyError(
        `A product's ${label} must be at least 2 characters long.`
      )
    }
    return name
  }

  function validatePrice(label: string, price: number) {
    if (isNaN(price)) {
      throw new InvalidPropertyError(
        `A product's ${label} must be type of number.`
      )
    }
    return Number(price)
  }

  function normalize ({ ...otherInfo }: IProduct) {
    return {
      ...otherInfo
    }
  }
}