import express from 'express';
import uploadImage from '../middleware/uploadImage';
import productController from './productController';

const router = express.Router();

router.get('/', productController);
router.post('/', uploadImage.single('productImage'), productController);
router.get('/:productId', productController);

export default router;