export default interface IProduct {
  name: string,
  price: number,
  productImage: string
}