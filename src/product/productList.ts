import makeProduct from './product'
import IProduct from './productInterface'

export default function makeProductList({ database }: any) {
  return Object.freeze({
    add,
    findById,
    getItems
  })

  async function getItems ({ max = 100 } = {}) {
    const db = await database
    const query = {}
    const projection = {'name': 1, 'price':1, 'productImage': 1}
    const items = await db
      .collection('products')
      .find(query, {projection})
      .limit(Number(max))
      .toArray()
    return items.map(documentToProduct)
  }

  async function add ({...product }) {
    const db = await database
    const { result, ops } = await db
      .collection('products')
      .insertOne(product)
      .catch((mongoError: any) => {
        throw mongoError
      })
    return {
      success: result.ok === 1,
      created: documentToProduct(ops[0])
    }
  }

  async function findById ({ _id }: any) {
    const db = await database
    const projection = {'name': 1, 'price':1, 'productImage': 1}
    const found = await db
      .collection('products')
      .findOne({ _id: db.makeId(_id) }, {projection})
    if (found) {
      return documentToProduct(found)
    }
    return null
  }

  function documentToProduct ({ ...doc }: IProduct) {
    return doc;
  }
}