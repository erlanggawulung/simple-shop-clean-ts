import makeDb from '../db'
import makeProductList from './productList'
import makeProductEndPointHandler from './productEndPoint'

const database = makeDb()
const productList = makeProductList({ database })
const productEndPointHandler = makeProductEndPointHandler({ productList })

export default productEndPointHandler
