import {
  UniqueConstraintError,
  InvalidPropertyError,
  RequiredParameterError
} from '../helper/error'
import makeHttpError from '../helper/httpError'
import makeProduct from './product'

export default function makeProductsEndpointHandler ({ productList }: any) {
  return async function handle (httpRequest: any) {
    switch (httpRequest.method) {
      case 'POST':
        return postProduct(httpRequest)

      case 'GET':
        return getProducts(httpRequest)

      default:
        return makeHttpError({
          statusCode: 405,
          errorMessage: `${httpRequest.method} method not allowed.`
        })
    }
  }

  async function getProducts (httpRequest: any) {
    const { productId }: any = httpRequest.pathParams || {}
    const { max }: any = httpRequest.queryParams || {}
    
    const result = productId
      ? await productList.findById({ _id: productId })
      : await productList.getItems({ max })
    return {
      headers: {
        'Content-Type': 'application/json'
      },
      statusCode: 200,
      data: JSON.stringify(result)
    }
  }

  async function postProduct (httpRequest: any) {
    let productInfo = httpRequest.body
    if (!productInfo) {
      return makeHttpError({
        statusCode: 400,
        errorMessage: 'Bad request. No POST body.'
      })
    }

    if (typeof httpRequest.body === 'string') {
      try {
        productInfo = JSON.parse(productInfo)
      } catch {
        return makeHttpError({
          statusCode: 400,
          errorMessage: 'Bad request. POST body must be valid JSON.'
        })
      }
    }

    try {
      const {path} = httpRequest.file
      const {name, price} = httpRequest.body
      const productInfo = {
        name,
        price,
        productImage: path
      }
      const product = makeProduct({productInfo})
      const result = await productList.add(product)
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: 201,
        data: JSON.stringify(result)
      }
    } catch (e) {
      return makeHttpError({
        errorMessage: e.message,
        statusCode:
          e instanceof UniqueConstraintError
            ? 409
            : e instanceof InvalidPropertyError ||
              e instanceof RequiredParameterError
              ? 400
              : 500
      })
    }
  }
}
