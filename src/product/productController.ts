import {Request, Response} from 'express';
import adaptRequest from '../helper/adaptRequest';
import handleProductRequest from '.';

export default async function productController (req: Request, res: Response) {
  const httpRequest = adaptRequest(req)
  try {
    const { headers, statusCode, data } = await handleProductRequest(httpRequest)
    return res
        .set(headers)
        .status(statusCode)
        .send(data)
  } catch (e) {
    console.error(e)
    return res.status(500).end()
  }
}