import {Request, Response, NextFunction} from 'express';
import jwt from 'jsonwebtoken';

const checkAuth = (httpRequest: any, res: Response, next: NextFunction) => {
    try {
        const token = httpRequest.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, `${process.env.JWT_KEY}`);
        httpRequest.userData = decoded;
        next();
    } catch(err) {
        console.log({err});
        return res.status(401).json({
            message: 'Auth failed.'
        });
    }
}

export default checkAuth;