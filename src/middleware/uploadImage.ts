import {Request} from 'express';
import multer from 'multer';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
      cb(null, './uploads/');
  },
  filename: (req, file, cb) => {
      cb(null, new Date().toISOString() + "_" + file.originalname);  
  }
});

const fileFilter = (req: Request, file: any, cb: any) => {
  if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
      cb(null, true);
  } else {
      cb(new Error('file mime type is not supported.'), false);
  }
}
const uploadImage = multer({storage: storage, limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

export default uploadImage;